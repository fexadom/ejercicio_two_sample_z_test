library(readxl)
library(plyr)
library(dplyr)
library(ggplot2)

#Carga los datos del archivo Excel
datos <- read_excel("reporte-14.xlsx", col_types = c("text","text", "numeric", "numeric","numeric", "numeric", "numeric", "numeric", "text"))
datos_a <- arrange(datos,ID,Tarea)

#agrega una columna extra con el número de intento de cada estudiante
intentos = rep(1,length(datos_a$ID))
c <- 0
inc <- 1
last_m <- ""
for(m in datos_a$ID) {
  c <- c + 1
  if (last_m == m){
    intentos[c] <- intentos[c] + inc
    inc <- inc + 1
  }else
    inc <- 1
  
  last_m <- m
}

#Convierte a intentos y retroalimentación en un factor
datos_a$Intento = factor(intentos)
datos_a$Retroalimentacion <- factor(datos_a$Retroalimentacion)

datos_grupos = filter(datos_a,Retroalimentacion != 'ambos')
estudiantes_2_intentos_grupos <- filter(datos_grupos,Intento == 2)